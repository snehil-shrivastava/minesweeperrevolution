package team.seven.minesweeper.microserver.server.observers

import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import team.seven.minesweeper.microserver.commons.BaseSchedulerProvider
import team.seven.minesweeper.microserver.server.ClientHandler
import team.seven.minesweeper.microserver.server.Server
import team.seven.minesweeper.microserver.server.ServerImpl
import timber.log.Timber
import java.net.Socket

internal class SocketObserver(
    private val server: ServerImpl,
    private val schedulers: BaseSchedulerProvider
    ) : Observer<Socket> {

    private val clientHandles = ArrayList<ClientHandler>()
    private val compositeDisposable = CompositeDisposable()


    override fun onSubscribe(d: Disposable) {
        compositeDisposable.add(d)
    }

    override fun onError(e: Throwable) {
        Timber.e(e, "Failed to connect to client :: ")
    }

    override fun onComplete() {
        Timber.i("Successfully executed")
    }

    override fun onNext(t: Socket) {
        clientHandles.add(ClientHandler(server, t, schedulers))
    }

    fun close(){
        compositeDisposable.clear()
    }
}