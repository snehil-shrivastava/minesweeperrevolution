package team.seven.minesweeper.microserver.commons

import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.TestScheduler

class TestSchedulerRequester {

    val schedulers = ArrayList<TestScheduler>()

    fun createNew(): Scheduler {
        val t =  TestScheduler()
        schedulers.add(t)
        return t
    }



    fun closeAll(){
        schedulers.forEach {
            it.shutdown()
        }
    }

    fun triggerActions() {
        schedulers.forEach {
            Thread {
                it.triggerActions()
            }.start()
        }
    }

}
