package team.seven.minesweeper.microserver.game

import team.seven.minesweeper.microserver.client.Client
import team.seven.minesweeper.microserver.obj.*

internal class GameControllerImpl
    (private val client: Client,
     private val clientObject: ClientObject
     ) : GameController {

    private lateinit var gameConfig: GameConfig
    private lateinit var gameData: GameData
    private var userEvents: UserEvents? = null

    override fun revealTile(coordinates: Pair<Int, Int>){
        val  state = gameData.revealTile(coordinates)
        if(state == ClientState.LOST){
            userEvents?.lost()
            client.lost(clientObject)
        }else if(state ==  ClientState.WON){
            client.won(clientObject)
            userEvents?.won()
        }
    }

    override fun sendMine(coordinates: Pair<Int, Int>, opponent: String) {
        client.sendMine(coordinates, opponent)
    }

    override fun showResults() {
        userEvents?.showResults()
    }

    override fun addMine(coordinates: Pair<Int, Int>) {
        gameData.setMine(coordinates)
    }

    override fun waitingRoomStatusChanged(joinedPlayers: Int, totalPlayers: Int) {
        userEvents?.waitingRoomStatusChanged(joinedPlayers, totalPlayers)
    }

    override fun getHintMap(coordinates: Pair<Int, Int>): Array<Array<Int>> {
        return gameData.getHintMap(coordinates)
    }

    override fun timeout() {
        userEvents?.timeout()
    }

    override fun setMineMap(mineMap: Array<Array<Int>>) {
        gameData.setMineMap(mineMap)
        client.sendAck(clientObject.uid, ACK.RECEIVED_MINE_MAP)
    }

    override fun showHintMap(coordinates: Pair<Int, Int>, hintMap: Array<Array<Int>>) {
        userEvents?.showHintMap(coordinates, hintMap)
    }

    override fun notifyPlayerHasQuitted(uid: String) {
        //
    }

    override fun updateRankList(rankList: ArrayList<String>) {
        TODO()
    }

    override fun setGameConfig(gameConfig: GameConfig) {
        this.gameConfig = gameConfig
        val size = gameConfig.boardSize
        gameData = GameData(gameConfig, Array(size){ i-> Array(size){j -> userEvents?.getITile(i,j)} })
    }

    override fun onGameJoinedSuccessfully() {
        userEvents?.joinSuccess()
    }

    override fun getGameConfig(): GameConfig {
        if(this::gameConfig.isInitialized){
            return gameConfig
        }
        throw Exception("Game config is not set")
    }

    override fun plantMineMap(opponent: String) {
        userEvents?.plantMineMap(opponent)
    }

    override fun waitForMove() {
        userEvents?.waitForMove()
    }

    override fun revealTileRequest() {
        userEvents?.revealTileRequest()
    }

    override fun plantTileForOpponent(opponent: String) {
        userEvents?.plantTileForOpponent(opponent)
    }

    override fun setOnUserEvent(event: UserEvents) {
        userEvents = event
    }

    override fun deregisterOnUserEvent() {
        userEvents = null
    }

}