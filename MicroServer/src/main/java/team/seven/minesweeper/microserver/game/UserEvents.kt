package team.seven.minesweeper.microserver.game

import team.seven.minesweeper.microserver.obj.ITile
import java.util.concurrent.CompletableFuture

interface UserEvents {
    fun lost()
    fun won()
    fun waitingRoomStatusChanged(joinedPlayers: Int, totalPlayers: Int)
    fun timeout()
    fun showHintMap(coordinates: Pair<Int, Int>, hintMap: Array<Array<Int>>)
    fun waitForMove()
    fun revealTileRequest()
    fun plantTileForOpponent(opponent: String)
    fun joinSuccess()
    fun plantMineMap(opponent: String)
    fun getITile(x: Int, y: Int) : ITile
    fun showResults()
}