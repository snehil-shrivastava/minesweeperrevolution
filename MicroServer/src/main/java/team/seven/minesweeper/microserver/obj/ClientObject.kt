package team.seven.minesweeper.microserver.obj


data class ClientObject(val uid: String, var isOnline: Boolean) {
    fun isOnline(b: Boolean) {
        isOnline = b
    }
}