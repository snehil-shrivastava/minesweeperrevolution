package team.seven.minesweeper.microserver.client

import com.google.gson.internal.LinkedTreeMap
import team.seven.minesweeper.microserver.commons.*
import team.seven.minesweeper.microserver.game.GameController
import team.seven.minesweeper.microserver.obj.*
import team.seven.minesweeper.microserver.util.Utils
import timber.log.Timber
import java.net.Socket
import java.text.ParseException


internal class ServerHandler(
    private val gameController: GameController,
    socket: Socket,
    schedulers: BaseSchedulerProvider
) : RxIOs(socket.dataInputStream, socket.dataOutputStream, schedulers) {

    init {
        reader?.subscribe(this)
    }

    override fun onDisconnect() {
        Timber.i("Disconnected from server")
        compositeDisposable.dispose()
    }

    override fun onNext(t: String) {
        val map = Utils.gson.fromJson(t, Map::class.java)
        val code = (map["code"] as? String)?.let { ClientCode.valueOf(it) }
        val errorCode = (map["errorCode"] as? String)?.let { ClientErrorCodes.valueOf(it) }
        when {
            (code == null && errorCode == null) -> {
                Timber.e(ParseException("code and errorCode both are not found", 0))
                return
            }
            (code != null && errorCode != null) -> {
                Timber.e(ParseException("cannot have both code and errorCode", 0))
                return
            }
            (errorCode != null) -> {
                handleError(errorCode)
            }
            (code != null) -> {
                handleCode(code, map)
            }
        }


    }

    private fun handleCode(code: ClientCode, map: Map<*, *>) {
        when(code){
            ClientCode.ADD_MINE -> {
                val coordinates = getCoordinates(map)
                gameController.addMine(coordinates)
            }
            ClientCode.WAITING_ROOM_STATUS_CHANGED -> {
                val joinedPlayers = map["joinedPlayers"] as? Int ?: throw Exception("Failed to get the no of players joined")
                val totalPlayers =  map["totalPlayers"] as? Int ?: throw Exception("Failed to get the total no of players joined")
                gameController.waitingRoomStatusChanged(joinedPlayers, totalPlayers)
            }
            ClientCode.HINT_MAP_REQUEST -> {
                val coordinates = getCoordinates(map)
                val hintMap = gameController.getHintMap(coordinates)
                sendHint(coordinates, hintMap)
            }
            ClientCode.TIMEOUT -> {
                gameController.timeout()
            }
            ClientCode.SEND_MINE_MAP -> {
                val mineMap = getMap(map, "mineMap", gameController.getGameConfig().boardSize)
                gameController.setMineMap(mineMap)
            }
            ClientCode.SEND_HINT_MAP -> {
                val coordinates = getCoordinates(map)
                val hintMap = getMap(map, "hintMap", gameController.getGameConfig().hintMapSize)
                gameController.showHintMap(coordinates, hintMap)
            }
            ClientCode.PLAYER_QUITTED -> {
                val uid = map["uid"] as? String ?: throw Exception("Failed to get the player id who has quitted the game")
                gameController.notifyPlayerHasQuitted(uid)
            }
            ClientCode.RANK_LIST -> {
                val rankList = getRankList(map)
                gameController.updateRankList(rankList)
            }
            ClientCode.JOIN_SUCCESS -> {
                val gameConfig = getGameConfig(map["gameConfig"]) ?: throw  Exception("Failed to get game config when joined")
                gameController.setGameConfig(gameConfig)
                gameController.onGameJoinedSuccessfully()
            }
            ClientCode.PLANT_MINE -> {
                val opponent = map["opponent"] as? String ?: throw  Exception("Failed to get opponent")
                gameController.plantTileForOpponent(opponent)
            }
            ClientCode.PLANT_MINE_MAP -> {
                val opponent = map["opponent"] as? String ?: throw  Exception("Failed to get opponent")
                gameController.plantMineMap(opponent)
            }
            ClientCode.WAIT_FOR_MOVE -> {
                gameController.waitForMove()
            }
            ClientCode.REVEAL -> {
                gameController.revealTileRequest()
            }
            ClientCode.SHOW_RESULTS -> {
             gameController.showResults()
            }
            ClientCode.PLAYER_JOINED -> {

            }
        }
    }

    private fun sendHint(coordinates: Pair<Int, Int>, hintMap: Array<Array<Int>>) {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.HINT
        map["coordinates"] = coordinates
        map["hintMap"] = hintMap
        write(Utils.gson.toJson(map))
    }

    private fun handleError(errorCode: ClientErrorCodes) {
        when(errorCode){
            ClientErrorCodes.SERVER_FULL -> {

            }
            ClientErrorCodes.CANNOT_JOIN -> {

            }
            ClientErrorCodes.FORCE_CLOSED -> {

            }
            ClientErrorCodes.INVALID_COORDINATES -> {

            }
            ClientErrorCodes.INVALID_MINE_MAP -> TODO()
            ClientErrorCodes.INVALID_COMMAND -> TODO()
            ClientErrorCodes.INVALID_OPPONENT -> TODO()
            ClientErrorCodes.INVALID_ACK -> {
                println("Invalid ACK Received")
            }
            ClientErrorCodes.ILLEGAL_STATE_ERROR -> TODO()
            ClientErrorCodes.TOO_MANY_TIMEOUT -> TODO()
            ClientErrorCodes.CANNOT_PLAY -> TODO()
        }
    }

    override fun onError(e: Throwable) {
        println(e)
    }

    override fun onComplete() {
        TODO("Not yet implemented")
    }

    fun joinGame(clientObject: ClientObject) {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.JOIN
        map["id"] = clientObject.uid
        write(Utils.gson.toJson(map))
    }

    fun quitGame(clientObject: ClientObject) {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.LEAVE
        map["id"] = clientObject.uid
        write(Utils.gson.toJson(map))
    }

    fun lostGame(clientObject: ClientObject) {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.LOST
        map["id"] = clientObject.uid
        write(Utils.gson.toJson(map))
    }

    fun wonGame(clientObject: ClientObject) {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.WON
        map["id"] = clientObject.uid
        write(Utils.gson.toJson(map))
    }

    fun sendMineMap(opponent: String, mineMap: Array<Array<Int>>) {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.MINE_MAP
        map["mineMap"] = mineMap
        map["opponent"] = opponent
        write(Utils.gson.toJson(map))
    }

    fun sendAck(clientId: String, ack: ACK) {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.ACK
        map["id"] = clientId
        map["ackCode"] = ack
        write(Utils.gson.toJson(map))
    }

    fun sendMine(coordinate: Pair<Int, Int>, opponent: String) {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.PLANT
        map["opponent"] = opponent
        map["coordinate"] = coordinate
        write(Utils.gson.toJson(map))
    }

}
