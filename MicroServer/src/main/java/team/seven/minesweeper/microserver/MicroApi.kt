package team.seven.minesweeper.microserver

import team.seven.minesweeper.microserver.client.Client
import team.seven.minesweeper.microserver.client.ClientImpl
import team.seven.minesweeper.microserver.commons.BaseSchedulerProvider
import team.seven.minesweeper.microserver.game.GameController
import team.seven.minesweeper.microserver.game.GameControllerImpl
import team.seven.minesweeper.microserver.obj.ClientObject
import team.seven.minesweeper.microserver.obj.NgrokProp
import team.seven.minesweeper.microserver.server.Server
import team.seven.minesweeper.microserver.server.ServerImpl

object MicroApi {

    fun getServer( schedulers: BaseSchedulerProvider, ngrok: NgrokProp, ngrokUrlCreatedCallback: (Pair<String, String>) -> Unit) : Server {
        return ServerImpl( schedulers, ngrok, ngrokUrlCreatedCallback)
    }

    fun getClient(schedulers: BaseSchedulerProvider) : Client {
        return ClientImpl(schedulers)
    }

    fun  getGameController(client: Client, clientObject: ClientObject) : GameController {
        return GameControllerImpl(client, clientObject)
    }

}