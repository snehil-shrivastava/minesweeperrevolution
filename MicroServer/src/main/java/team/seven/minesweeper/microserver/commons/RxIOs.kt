package team.seven.minesweeper.microserver.commons

import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import okio.sink
import timber.log.Timber
import java.io.DataInputStream
import java.io.DataOutputStream

internal abstract class RxIOs(
    private val inputStream: DataInputStream,
    private val outputStream: DataOutputStream,
    private val schedulers: BaseSchedulerProvider
) : Observer<String> {

    val compositeDisposable = CompositeDisposable()

    val reader = Observable.create<String> {
        var run = true
        while (run) {
            try {
                val command = inputStream.readUTF()
                it.onNext(command)
            } catch (e: Exception) {
                it.onError(Exception("Failed to read command from input stream", e))
                onDisconnect()
                run = false
            }
        }
    }.observeOn(schedulers.ui())?.subscribeOn(schedulers.io())

    private val observable: PublishSubject<String> = PublishSubject.create()

    fun write(value: String?) {
        value?.let { observable.onNext(value) }
    }

    init {
        val writer = observable.toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(Schedulers.io())
            .map {
                try {
                    outputStream.writeUTF(it+"\n")
                } catch (e: Exception) {
                    return@map e.message
                }
                it
            }
            .subscribe({
                // Ignored
            }, {
                print(it.message)
            })
        compositeDisposable.add(writer)
    }

    abstract fun onDisconnect()

    override fun onSubscribe(d: Disposable) {
        compositeDisposable.add(d)
    }

    abstract override fun onNext(t: String)

    abstract override fun onError(e: Throwable)

    abstract override fun onComplete()
}