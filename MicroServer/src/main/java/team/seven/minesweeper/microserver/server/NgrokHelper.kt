package team.seven.minesweeper.microserver.server

import android.annotation.SuppressLint
import android.os.Build
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import team.seven.minesweeper.microserver.commons.NGROK_TUNNEL_API_URL
import team.seven.minesweeper.microserver.obj.NgrokProp
import team.seven.minesweeper.microserver.obj.Root
import java.io.File
import java.io.IOException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


internal class NgrokHelper(private val port: Int, private val props: NgrokProp){
    private lateinit var ngrok : Process
    private val env = arrayOf("HOME=${props.home}")

    fun openPortToInternet(): Pair<String, String>{
        var retVal = Pair("", "")

        if( isNgrokRunning() )
            throw IllegalAccessException("Ngrok is already running")

        try {
            setUpNgrok()

            ngrok = Runtime.getRuntime().exec("${props.path} tcp $port --log=stdout", env)

            // sleeping for 20 seconds
            Thread.sleep(4000)

            getPortFromNgrokApi()?.let { retVal = it }

        } catch (e: IOException) {
            e.printStackTrace()
            throw Exception(e)
        } catch (e: InterruptedException) {
            throw Exception(e)
        }
        return retVal
    }

    private fun setUpNgrok() {
        if(File("${props.home}\\.ngrok2\\ngrok.yml").exists().not()){
            ngrok = Runtime.getRuntime().exec("${props.path} authtoken ${props.token} --log=stdout", env)
            ngrok.waitFor()
        }
    }

    private fun getPortFromNgrokApi(): Pair<String, String>? {
        val client: OkHttpClient = getUnsafeOkHttpClient()
        val request: Request = Request.Builder()
            .url(NGROK_TUNNEL_API_URL)
            .get()
            .addHeader("Accept", "application/json")
            .build()
        val response: Response = client.newCall(request).execute()
        val root = Gson().fromJson(response.body?.string(), Root::class.java)
        val x = root?.tunnels?.get(0)?.public_url

        x?.let {
            val value = x.substring(6).split(":")
            return Pair(value[0], value[1])
        }

        return null
    }

    private fun isNgrokRunning(): Boolean {
        return if (this::ngrok.isInitialized) {
            try {
                ngrok.exitValue()
                false
            } catch (e: IllegalThreadStateException) {
                true
            }
        }else {
            false
        }
    }

    private fun getUnsafeOkHttpClient(): OkHttpClient {
        // Create a trust manager that does not validate certificate chains
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            @SuppressLint("TrustAllX509TrustManager")
            override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
            }

            override fun getAcceptedIssuers() = arrayOf<X509Certificate>()
        })

        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory = sslContext.socketFactory

        return OkHttpClient.Builder()
            .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            .hostnameVerifier { _, _ -> true }.build()
    }

    fun closePortToInternet(){
        if(this::ngrok.isInitialized.not()) return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ngrok.destroyForcibly()
        }else{
            ngrok.destroy()
        }
    }

}