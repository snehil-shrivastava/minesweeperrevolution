package team.seven.minesweeper.microserver.server.utils

import team.seven.minesweeper.microserver.obj.ACK
import team.seven.minesweeper.microserver.server.ServerImpl

internal class AcknowledgementHandler(private val server: ServerImpl) {

    lateinit var clientList: Set<String>

    private val playersThatHavePlantedMine = HashSet<String>()
    private val playersThatHaveReceivedMineMap = HashSet<String>()

    var hasPlayerRevealedTile = false

    fun handle(id: String, code: ACK) : Boolean {
        val toReturn = verify(code)
        if(toReturn){
            when(code) {
                ACK.PLANTED_MINE -> {
                    playersThatHavePlantedMine.add(id)
                }
                ACK.RECEIVED_MINE_MAP -> {
                    // ? what will happen if someone didn't send mine map
                    // ? resolve congestion
                    playersThatHaveReceivedMineMap.add(id)
                    if(playersThatHaveReceivedMineMap.filter { clientList.contains(it) }.size == clientList.size){
                        server.startPlaying()
                    }
                }
                ACK.REVEALED_TILE -> {
                    hasPlayerRevealedTile = true
                }
            }
        }
        return toReturn
    }

    fun hasPlantedMineForCurrentOpponent(id: String): Boolean {
        return playersThatHavePlantedMine.contains(id)
    }

    private fun verify(code: ACK) : Boolean{
        return when(code){
            ACK.PLANTED_MINE, ACK.REVEALED_TILE  -> {
                server.gameState == ServerImpl.GameState.PLAYING
            }
            ACK.RECEIVED_MINE_MAP -> {
                server.gameState == ServerImpl.GameState.PLAYING_MAPPING
            }
        }
    }

    fun getPlayersWhoDidNotSendMine(): List<String> {
        return  clientList.filter { x -> playersThatHavePlantedMine.contains(x).not() && x != server.turnHandler.getCurrentPlayer() && server.isNotPlaying(x) }
    }

    fun haveSendMineMap(id: String) : Boolean = playersThatHaveReceivedMineMap.contains(id)
}
