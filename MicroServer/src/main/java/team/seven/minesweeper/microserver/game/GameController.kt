package team.seven.minesweeper.microserver.game

import team.seven.minesweeper.microserver.obj.GameConfig

interface GameController {

    fun addMine(coordinates: Pair<Int, Int>)

    fun waitingRoomStatusChanged(joinedPlayers: Int, totalPlayers: Int)

    fun getHintMap(coordinates: Pair<Int, Int>): Array<Array<Int>>

    fun timeout()

    fun setMineMap(mineMap: Array<Array<Int>>)

    fun showHintMap(coordinates: Pair<Int, Int>, hintMap: Array<Array<Int>>)

    fun notifyPlayerHasQuitted(uid: String)

    fun updateRankList(rankList: ArrayList<String>)

    fun setGameConfig(gameConfig: GameConfig)

    fun onGameJoinedSuccessfully()

    fun getGameConfig(): GameConfig

    fun plantMineMap(opponent: String)

    fun waitForMove()

    fun revealTileRequest()

    fun plantTileForOpponent(opponent: String)

    fun setOnUserEvent(event: UserEvents)

    fun deregisterOnUserEvent()

    fun revealTile(coordinates: Pair<Int, Int>)

    fun sendMine(coordinates: Pair<Int, Int>, opponent: String)

    fun showResults()
}