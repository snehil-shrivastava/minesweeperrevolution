package team.seven.minesweeper.microserver.server

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import team.seven.minesweeper.microserver.commons.*
import team.seven.minesweeper.microserver.obj.*
import team.seven.minesweeper.microserver.server.observers.NgrokObserver
import team.seven.minesweeper.microserver.server.observers.SocketObserver
import team.seven.minesweeper.microserver.server.utils.AcknowledgementHandler
import team.seven.minesweeper.microserver.server.utils.OpponentVerification
import team.seven.minesweeper.microserver.server.utils.TurnHandler
import timber.log.Timber
import java.net.ServerSocket
import java.net.Socket
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

internal class ServerImpl(
    private val schedulers: BaseSchedulerProvider,
    private val props: NgrokProp,
    private val ngrokUrlCreatedCallback: (Pair<String, String>) -> Unit
) : Server {

    internal enum class GameState {
        PLAYING_MAPPING, PLAYING, LOBBY, FINISHED
    }

    lateinit var config: GameConfig
        private set

    private lateinit var serverSocket: ServerSocket
    private lateinit var ngrokHelper: NgrokHelper

    private lateinit var ngrokObservable: Observable<Pair<String, String>>
    private lateinit var socketsObservable: Observable<Socket>
    lateinit var gameState: GameState

    private val clientList = HashMap<String, ClientObject>()
    private val clients = HashMap<String, GamePlayListener>()

    private val socketObserver = SocketObserver(this, schedulers)

    private val winners = HashSet<String>()
    private val losers = HashSet<String>()

    private val clientArrayList = ArrayList<String>()

    val ackHandler = AcknowledgementHandler(this)
    lateinit var turnHandler: TurnHandler
        private set
    lateinit var opponentVerification: OpponentVerification
        private set

    private val minesMap = HashMap<String, Array<Array<Int>>>()
    private lateinit var opponentsMap: Map<String, String>

    private val compositeDisposable = CompositeDisposable()

    @Volatile
    private var isExecuted = false

    override fun initialize(configuration: GameConfig) {
        gameState = GameState.LOBBY
        serverSocket = ServerSocket(2345)
        ngrokHelper = NgrokHelper(serverSocket.localPort, props)
        ngrokObservable = getNgrokObservable()
        socketsObservable = getSocketObservable(serverSocket)
        config = configuration
        ngrokObservable.observeOn(schedulers.ui()).subscribeOn(schedulers.io()).firstElement()
            .subscribe(
                NgrokObserver {
                    socketsObservable.observeOn(schedulers.ui()).subscribeOn(schedulers.io())
                        .subscribe(socketObserver)
                    ngrokUrlCreatedCallback(it)
                }
            )
    }

    private fun getSocketObservable(serverSocket: ServerSocket): Observable<Socket> =
        Observable.create {
            while (true) {
                try {
                    val socket = serverSocket.accept()
                    it.onNext(socket)
                } catch (e: Exception) {
                    it.onSafeError(e)
                }
            }
        }

    private fun getNgrokObservable(): Observable<Pair<String, String>> = Observable.create {
        try {
            val item = ngrokHelper.openPortToInternet()
            it.onNext(item)
        } catch (e: Exception) {
            e.printStackTrace()
            it.onSafeError(e)
        }
    }

    override fun join(clientObject: ClientObject, gamePlayListener: GamePlayListener) {
        if (gameState == GameState.PLAYING_MAPPING || gameState == GameState.PLAYING) {
            gamePlayListener.throwError(ClientErrorCodes.CANNOT_JOIN, ERROR_JOIN_GAME_ON)
        } else if (gameState == GameState.FINISHED) {
            gamePlayListener.throwError(ClientErrorCodes.CANNOT_JOIN, ERROR_JOIN_GAME_FINISHED)
        } else if (clientList.size == config.playersCount) {
            gamePlayListener.throwError(ClientErrorCodes.SERVER_FULL, ERROR_JOIN_FULL)
        } else {
            if (clientList.containsKey(clientObject.uid)) {
                gamePlayListener.throwError(ClientErrorCodes.CANNOT_JOIN, ERROR_JOIN_ALREADY_JOINED)
                return
            }
            clients.mapKeys {
                it.value.notifyPlayerHasJoined(clientObject.uid)
            }
            clientList[clientObject.uid] = clientObject
            clients[clientObject.uid] = gamePlayListener
            clientArrayList.add(clientObject.uid)
            gamePlayListener.successfullyJoinedGame(config, clientArrayList)
            if (clientList.size == config.playersCount) {
                opponentsMap = generateDerangementMap(clientArrayList)
                clients.mapKeys {
                    opponentsMap[it.key]?.let { opponent ->
                        it.value.onStart(opponent)
                    }
                }
                opponentVerification = OpponentVerification(opponentsMap)

                compositeDisposable.add(
                    Observable.just("")
                        .delay(config.timeout, TimeUnit.SECONDS)
                        .observeOn(schedulers.ui()).subscribeOn(schedulers.io())
                        .subscribe {

                            doOnMineMapReceived()

                        }
                )
            }
        }
    }

    private fun resolveAndSendMineMap(x: Map<String, String>, noMap: Set<String>) {
        val arrayList = ArrayList<String>()
        var curr = x.keys.iterator().next()
        val old = curr

        arrayList.add(old)
        curr = x[curr] ?: return
        while (curr != old) {
            if (noMap.contains(curr).not())
                arrayList.add(curr)
            curr = x[curr] ?: return
        }
        val size = arrayList.size
        if (size > 1) {
            for (i in 0 until size) {
                minesMap[arrayList[i]]?.let {
                    clients[arrayList[(i + 1) % size]]?.sendMineMap(it)
                }
            }
            ackHandler.clientList = HashSet(arrayList)
            gameState = GameState.PLAYING_MAPPING
        } else if (arrayList.size == 1) {
            clients[arrayList[0]]?.throwError(
                ClientErrorCodes.CANNOT_PLAY,
                "No players are left to play"
            )
            gameState = GameState.FINISHED
        }
    }

    override fun quit(id: String) {
        if (clients.containsKey(id)) {
            clients[id]?.onEnd()
            clients.remove(id)
            clients.values.forEach {
                it.notifyPlayerHasQuitted(id)
            }
            clientList[id]?.isOnline(false)
        }
    }

    override fun close() {
        // * should call two method what if the game is on but server is closing
        // ? what should other client do... probably we have to report an error
        if (gameState == GameState.FINISHED) {
            clients.values.forEach {
                it.onEnd()
            }
        } else {
            clients.values.forEach {
                it.throwError(ClientErrorCodes.FORCE_CLOSED, ERROR_CLOSE_FORCED)
            }
        }

        if (this::serverSocket.isInitialized)
            serverSocket.close()

        if (this::ngrokHelper.isInitialized)
            ngrokHelper.closePortToInternet()
    }

    override fun plantMine(opponent: String, coordinates: Pair<Int, Int>) {
        clients[opponent]?.plantMine(coordinates)
            ?: Timber.e("Client not found while sending plant mine")
    }

    override fun sendMineMap(sender: String, opponent: String, map: Array<Array<Int>>) {
        minesMap[sender] = map
        if (minesMap.filter { clientList.containsKey(it.key) }.size == config.playersCount) {
            doOnMineMapReceived()
        }
    }

    private fun doOnMineMapReceived() {
        if (isExecuted) return
        val noMap = clients.filter { ackHandler.haveSendMineMap(it.key) }.keys
        noMap.forEach {
            clients[it]?.throwError(ClientErrorCodes.TOO_MANY_TIMEOUT, "too long to send mine map")
        }
        resolveAndSendMineMap(opponentsMap, noMap)
        isExecuted = true
    }

    override fun sendHintMap(
        requestMaker: String,
        coordinates: Pair<Int, Int>,
        hintMap: Array<Array<Int>>
    ) {
        clients[requestMaker]?.sendHintMap(coordinates, hintMap)
            ?: Timber.e("Client not found while sending hint map")
    }

    override fun requestHintMap(opponent: String, coordinates: Pair<Int, Int>) {
        clients[opponent]?.requestHintMap(coordinates)
            ?: Timber.e("Client not found while sending hint map")
    }

    override fun lostGame(id: String) {
        if(gameState != GameState.PLAYING)
            return
        when {
            losers.contains(id) -> {
                Timber.e(Exception("Player has already lost"))
            }
            winners.contains(id) -> {
                Timber.e(Exception("Player has already won"))
            }
            else -> {
                losers.add(id)
                if(this::turnHandler.isInitialized) turnHandler.notPlayingGame(id)
            }
        }
    }

    override fun wonGame(id: String) {
        if(gameState != GameState.PLAYING)
            return
        when {
            losers.contains(id) -> {
                Timber.e(Exception("Player has already lost"))
            }
            winners.contains(id) -> {
                Timber.e(Exception("Player has already won"))
            }
            else -> {
                winners.add(id)
                if(this::turnHandler.isInitialized) turnHandler.notPlayingGame(id)
            }
        }
    }

    override fun sendRankList(requestMaker: String) {
        clients[requestMaker]?.sendRankList(winners, losers)
    }

    // to be used in background thread
    override fun startPlaying() {
        gameState = GameState.PLAYING
        readyPlayer()
        val penalty = HashMap<String, Int>()
        // game loop
        val dispose = Observable.create<Boolean> {
            executeGameLoop(penalty)
            it.onNext(turnHandler.isGameGoingOn)
        }.takeWhile { it }
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())
            .subscribe {
                if (it)
                    endAllClients()
            }
        compositeDisposable.add(dispose)
    }

    private fun endAllClients() {
        clients.forEach {
            it.value.onEnd()
        }
    }

    private fun readyPlayer() {
        clients.values.forEach {
            it.acceptPlayCommands()
        }
        turnHandler = TurnHandler(clientArrayList)
    }

    fun executeGameLoop(penalty: HashMap<String, Int>) {
        val currentPlayer = turnHandler.getCurrentPlayer()

        clients[currentPlayer]?.waitForMove(Wait.PLANT, "ALL")
        clients.filter { it.key != currentPlayer && isNotPlaying(it.key) }
            .forEach {
                it.value.sendPlantMineRequest(currentPlayer)
            }

        Thread.sleep(config.timeout * 100)

        ackHandler.getPlayersWhoDidNotSendMine().filter { isNotPlaying(it) }.forEach {
            increasePenaltyAndSendTimeout(it, penalty)
        }
        ackHandler.hasPlayerRevealedTile = false
        clients[currentPlayer]?.sendRevealTileRequest()

        clients.filter { it.key != currentPlayer && isNotPlaying(it.key) }
            .forEach { it.value.waitForMove(Wait.REVEAL, currentPlayer) }

        Thread.sleep(config.timeout * 100)

        if (ackHandler.hasPlayerRevealedTile.not()) {
            increasePenaltyAndSendTimeout(currentPlayer, penalty)
        }
        penalty.filter { (it.value >= config.maxTimeouts) }.keys.forEach { t ->
            clients[t]?.throwError(
                ClientErrorCodes.TOO_MANY_TIMEOUT,
                "Didn't respond for too long"
            )
            turnHandler.notPlayingGame(t)
            quit(t)
        }
        turnHandler.next()
    }

    private fun increasePenaltyAndSendTimeout(
        it: String,
        penalty: HashMap<String, Int>
    ) {
        clients[it]?.timeout()
        if (penalty[it] == null) penalty[it] = 0
        penalty[it]?.let { x -> penalty[it] = x + 1 }
    }

    fun isNotPlaying(x: String): Boolean = winners.contains(x).not() || losers.contains(x).not()

}

