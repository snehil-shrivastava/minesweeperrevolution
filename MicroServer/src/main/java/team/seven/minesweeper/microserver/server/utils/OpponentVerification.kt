package team.seven.minesweeper.microserver.server.utils

class OpponentVerification(private val map: Map<String, String>) {

    private var hasCreatedMineMap  = HashMap<String, Int>()

    fun check(player: String, opponent: String): Boolean {
        if(map[player] == opponent && hasCreatedMineMap[player] == null){
            hasCreatedMineMap[player] = 1
            return true
        }
        return false
    }
}