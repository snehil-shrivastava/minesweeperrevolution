package team.seven.minesweeper.microserver.obj

data class NgrokProp(val path: String, val home: String, val token: String)
