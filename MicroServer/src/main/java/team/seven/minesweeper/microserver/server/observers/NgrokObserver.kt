package team.seven.minesweeper.microserver.server.observers

import io.reactivex.rxjava3.core.MaybeObserver
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import timber.log.Timber

internal class NgrokObserver(private val callback: (Pair<String, String>) -> Unit) : MaybeObserver<Pair<String, String>> {

    private val compositeDisposable = CompositeDisposable()

    override fun onSubscribe(d: Disposable) {
        compositeDisposable.add(d)
    }

    override fun onError(e: Throwable) {
        Timber.e(e,"Failed to execute ngrok :: ")
    }

    override fun onComplete() {
        TODO("Not yet implemented")
    }

    override fun onSuccess(t: Pair<String, String>) {
        callback(t)
        Timber.i("Port opened at :: ${t.first} : ${t.second}")
    }

}
