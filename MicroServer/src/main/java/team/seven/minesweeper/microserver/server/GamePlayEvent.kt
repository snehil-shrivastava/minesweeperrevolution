package team.seven.minesweeper.microserver.server

import team.seven.minesweeper.microserver.obj.GameConfig
import team.seven.minesweeper.microserver.obj.Wait

interface GamePlayListener : GameLifeCycleListener {

    fun plantMine(coordinates: Pair<Int, Int>)

    fun requestHintMap(coordinates: Pair<Int, Int>)

    fun timeout()

    fun sendMineMap(mmap: Array<Array<Int>>)

    fun sendHintMap(coordinates: Pair<Int, Int>, hmap: Array<Array<Int>>)

    fun notifyPlayerHasQuitted(uid: String)

    fun sendRankList(winners: HashSet<String>, losers: HashSet<String>)

    fun successfullyJoinedGame(gameConfig: GameConfig, playerList: List<String>)

    fun sendPlantMineRequest(opponent: String)

    fun waitForMove(waitCode: Wait, forPlayer: String)

    fun sendRevealTileRequest()

    fun acceptPlayCommands()

    fun notifyPlayerHasJoined(uid: String)

}
