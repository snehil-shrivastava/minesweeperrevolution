package team.seven.minesweeper.microserver.server

import team.seven.minesweeper.microserver.obj.ClientErrorCodes

interface GameLifeCycleListener {

    /*
        [listeners, client] --- [
        server -> event -> send information to client using sockets -> client socket will receive these events and generate the same events
     *
     */

    fun onStart(opponent: String)

    // called when all other players had already won or lost and you are the last player still playing
    fun onEnd()

    fun throwError(code: ClientErrorCodes, s: String)

}

