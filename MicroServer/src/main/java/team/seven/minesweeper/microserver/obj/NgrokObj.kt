package team.seven.minesweeper.microserver.obj

// import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
// import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
/* ObjectMapper om = new ObjectMapper();
Root root = om.readValue(myJsonString), Root.class); */
class Config {
    var addr: String? = null
    var inspect = false
}

class Conns {
    var count = 0
    var gauge = 0
    var rate1 = 0
    var rate5 = 0
    var rate15 = 0
    var p50 = 0
    var p90 = 0
    var p95 = 0
    var p99 = 0
}

class Http {
    var count = 0
    var rate1 = 0
    var rate5 = 0
    var rate15 = 0
    var p50 = 0
    var p90 = 0
    var p95 = 0
    var p99 = 0
}

class Metrics {
    var conns: Conns? = null
    var http: Http? = null
}

class Tunnel {
    var name: String? = null
    var uri: String? = null
    var public_url: String? = null
    var proto: String? = null
    var config: Config? = null
    var metrics: Metrics? = null
}

class Root {
    var tunnels: List<Tunnel>? = null
    var uri: String? = null
}

