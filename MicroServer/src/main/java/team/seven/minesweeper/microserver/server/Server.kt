package team.seven.minesweeper.microserver.server

import team.seven.minesweeper.microserver.obj.ClientObject
import team.seven.minesweeper.microserver.obj.GameConfig

interface Server {

    // To initialize the server socket
    fun initialize(configuration: GameConfig)

    fun join(clientObject: ClientObject, gamePlayListener: GamePlayListener)

    fun quit(id: String)

    fun close()

    fun plantMine(opponent: String, coordinates: Pair<Int, Int>)

    fun sendMineMap(sender: String, opponent: String, map: Array<Array<Int>>)

    fun sendHintMap(requestMaker: String, coordinates: Pair<Int, Int>, hintMap: Array<Array<Int>>)

    fun requestHintMap(opponent: String, coordinates: Pair<Int, Int>)

    fun lostGame(id: String)

    fun wonGame(id: String)

    fun sendRankList(requestMaker: String)

    fun startPlaying()


}