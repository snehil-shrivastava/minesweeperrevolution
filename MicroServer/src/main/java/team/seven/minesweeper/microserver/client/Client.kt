package team.seven.minesweeper.microserver.client

import team.seven.minesweeper.microserver.game.GameController
import team.seven.minesweeper.microserver.obj.ACK
import team.seven.minesweeper.microserver.obj.ClientObject

interface Client {

    fun initialize(url: Pair<String, String>, gameController: GameController, callback: () -> Unit)

    fun join(clientObject: ClientObject)

    fun quit(clientObject: ClientObject)

    fun lost(clientObject: ClientObject)

    fun won(clientObject: ClientObject)

    fun sendMineMap(opponent: String, map: Array<Array<Int>>)

    fun sendAck(clientId: String, ack: ACK)

    fun sendMine(coordinate: Pair<Int, Int>, opponent: String)
}