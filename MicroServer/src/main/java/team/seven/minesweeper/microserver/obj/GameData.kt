package team.seven.minesweeper.microserver.obj

import team.seven.minesweeper.microserver.commons.checkCoordinates

class GameData(private val gameConfig: GameConfig, private val tiles: Array<Array<ITile?>>){

    lateinit var map: Array<Array<Int>>
        private set
    var flagged = empty(gameConfig.boardSize)
    var planted = empty(gameConfig.boardSize)
    var doubt = empty(gameConfig.boardSize)
    var reveled = empty(gameConfig.boardSize)

    private fun empty(boardSize: Int) = Array(boardSize){ Array(boardSize) {0} }

    var mineCount = 0
        private set
    var minePlantedCount = 0
        private set
    var flagCount = 0
        private set
    var doubtCount = 0
        private set
    var revealedCount = 0
        private set

    private lateinit var neighbor: Array<Array<Int>>

    private fun isMine(coordinate: Pair<Int, Int>) : Boolean {
        return if(checkCoordinates(coordinate, gameConfig))
            map[coordinate.first][coordinate.second] == 1
        else throw IndexOutOfBoundsException("Invalid coordinates")
    }

    private fun isReveled(coordinate: Pair<Int, Int>) : Boolean {
        return if(checkCoordinates(coordinate, gameConfig))
            reveled[coordinate.first][coordinate.second] == 1
        else throw IndexOutOfBoundsException("Invalid coordinates")
    }

    fun setMineMap(map: Array<Array<Int>>){
        if(this::map.isInitialized){
            throw Exception("Map is already set")
        }
        this.map = map
        findNeighbours()
    }

    // To plant in my board
    fun setMine(coordinate: Pair<Int, Int>){
        if (this::map.isInitialized.not())
            throw Exception("Map is not initialized")
        if(isMine(coordinate)) return
        if(isReveled(coordinate)) return
        setMapOn(coordinate, map, mineCount)
        updateNeighbours(coordinate)
    }

    private fun updateNeighbours(coordinate: Pair<Int, Int>) {
        val x = coordinate.first
        val y = coordinate.second
        if(x > 0 && y > 0){
            neighbor[x-1][y-1].inc()
            neighbor[x][y-1].inc()
            neighbor[x-1][y].inc()
            neighbor[x+1][y+1].inc()
            neighbor[x+1][y].inc()
            neighbor[x][y+1].inc()
            neighbor[x+1][y-1].inc()
            neighbor[x-1][y+1].inc()
        }else if(x > 0){
            neighbor[x-1][y].inc()
            neighbor[x+1][y+1].inc()
            neighbor[x+1][y].inc()
            neighbor[x][y+1].inc()
            neighbor[x-1][y+1]
        }else if(y > 0){
            neighbor[x][y-1].inc()
            neighbor[x+1][y+1].inc()
            neighbor[x+1][y].inc()
            neighbor[x][y+1].inc()
            neighbor[x+1][y-1].inc()
        }
    }


    private fun findNeighbours() {
        neighbor = Array(map.size){ i ->
            Array(map[0].size){ j->  countNeighbours(i, j) }
        }
    }

    private fun countNeighbours(i: Int, j: Int) : Int {
        return (map.at(i-1, j-1) +
                map.at(i, j-1) +
                map.at(i-1, j) +
                map.at(i+1,j+1) +
                map.at(i+1,j) +
                map.at(i,j+1) +
                map.at(i+1,j-1) +
                map.at(i-1,j+1))
    }



    private fun setMapOn(coordinate: Pair<Int, Int>, item: Array<Array<Int>>, count: Int) {
        setMapValue(coordinate, item, 1, count)
    }

    private fun setMapOff(coordinate: Pair<Int, Int>, item: Array<Array<Int>>, count: Int) {
        setMapValue(coordinate, item, 0, count)
    }

    private fun setMapValue(coordinate: Pair<Int, Int>, item: Array<Array<Int>>, on: Int, count: Int) {
        if (checkCoordinates(coordinate,gameConfig).not())
            throw IndexOutOfBoundsException("Invalid coordinates")
        item[coordinate.first][coordinate.second] = on
        count.inc()
    }

    // To plant in opponents board
    fun setPlantedMine(coordinate: Pair<Int, Int>){
        setMapOn(coordinate, planted, minePlantedCount)
    }

    fun flagTile(coordinate: Pair<Int, Int>){
        setMapOn(coordinate, flagged, flagCount)
    }

    fun markDoubtTile(coordinate: Pair<Int, Int>){
        setMapOn(coordinate, doubt, doubtCount)
    }


    fun unFlagTile(coordinate: Pair<Int, Int>){
        setMapOff(coordinate, flagged, flagCount)
    }

    fun unMarkDoubtTile(coordinate: Pair<Int, Int>){
        setMapOff(coordinate, doubt, doubtCount)
    }

    /***
     *  Return true when a mine is uncovered otherwise false
     */
    fun revealTile(coordinate: Pair<Int, Int>): ClientState {
        val x = coordinate.first
        val y = coordinate.second
        return if(reveled[x][y] == 1){
            ClientState.LOST
        }else{
            setMapOn(coordinate, reveled, revealedCount)
            floodFillUtil(coordinate, 50)
            if(revealedCount + this.getCorrectFlaggedCell() == gameConfig.boardSize.square()) {
                ClientState.WON
            }
            ClientState.PLAYING
        }
    }

    private fun getCorrectFlaggedCell(): Int {
        var count = 0
        flagged.forEachIndexed { i, v ->
            v.forEachIndexed { j, p ->
                if(p == 1 && map[i][j] == 1) count++
            }
        }
        return count
    }

    private fun floodFillUtil(coordinate: Pair<Int, Int>, delay: Int)
    {
        val x = coordinate.first
        val y = coordinate.second
        if(checkCoordinates(coordinate, gameConfig).not())
            return

        if (reveled[x][y] != 1)
            return

        // Replace the color at (x, y)
        reveled[x][y] = 1;
        tiles[x][y]?.animate(delay)
        delay.plus(100)

        // Recur for north, east, south and west
        floodFillUtil(Pair(x +1, y), delay);
        floodFillUtil(Pair(x -1, y), delay);
        floodFillUtil(Pair(x, y+1), delay);
        floodFillUtil( Pair(x, y-1), delay);
    }

    fun getHintMap(coordinates: Pair<Int, Int>): Array<Array<Int>> {
        val size = gameConfig.boardSize
        return Array(gameConfig.hintMapSize){ i ->
                Array(gameConfig.hintMapSize){ j ->
                    map[i%size][j%size]
                }
            }
    }

}

private fun Array<Array<Int>>.at(i: Int, j: Int) : Int{
    return try {
        this[i][j]
    } catch (e: java.lang.Exception) { 0 }
}

private fun Int.square(): Int {
    return this * this
}
