package team.seven.minesweeper.microserver.client

import io.reactivex.rxjava3.core.Observable
import team.seven.minesweeper.microserver.commons.BaseSchedulerProvider
import team.seven.minesweeper.microserver.game.GameController
import team.seven.minesweeper.microserver.obj.ACK
import team.seven.minesweeper.microserver.obj.ClientObject
import java.lang.Exception
import java.net.Socket

internal class ClientImpl(private val schedulers: BaseSchedulerProvider) : Client {

    private lateinit var socket: Socket
    private lateinit var serverHandler: ServerHandler

    override fun initialize(
        url: Pair<String, String>,
        gameController: GameController,
        callback: () -> Unit,
    ) {
        Observable.create<Socket> {
            try{
                val mSocket = Socket(url.first, url.second.toInt())
                it.onNext(mSocket)
            } catch (e: Exception) {
                it.tryOnError(e)
            }
        }.take(1).observeOn(schedulers.ui()).subscribeOn(schedulers.io()).subscribe {
            socket = it
            socket.keepAlive = true
            serverHandler = ServerHandler(gameController, socket, schedulers)
            callback()
        }
    }

    override fun join(clientObject: ClientObject) {
        if(this::serverHandler.isInitialized) serverHandler.joinGame(clientObject)
    }

    override fun quit(clientObject: ClientObject) {
        if(this::serverHandler.isInitialized) serverHandler.quitGame(clientObject)
    }

    override fun lost(clientObject: ClientObject) {
        if(this::serverHandler.isInitialized) serverHandler.lostGame(clientObject)
    }

    override fun won(clientObject: ClientObject) {
        if(this::serverHandler.isInitialized) serverHandler.wonGame(clientObject)
    }

    override fun sendMineMap(opponent: String, map: Array<Array<Int>>) {
        if(this::serverHandler.isInitialized) serverHandler.sendMineMap(opponent, map)
    }

    override fun sendAck(clientId: String, ack: ACK) {
        if(this::serverHandler.isInitialized) serverHandler.sendAck(clientId, ack)
    }

    override fun sendMine(coordinate: Pair<Int, Int>, opponent: String) {
        if(this::serverHandler.isInitialized) serverHandler.sendMine(coordinate, opponent)
    }

}