package team.seven.minesweeper.microserver.util

import com.google.gson.Gson

internal object Utils {
    @JvmStatic
    val gson = Gson()
}