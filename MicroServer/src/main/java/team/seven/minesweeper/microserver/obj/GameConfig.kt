package team.seven.minesweeper.microserver.obj

data class GameConfig(
    val playersCount: Int,
    val boardSize: Int,
    val hintMapSize: Int,
    val noOfMinesToBePlanted: Int,
    val timeout: Long, // in minutes
    val maxTimeouts: Int
)