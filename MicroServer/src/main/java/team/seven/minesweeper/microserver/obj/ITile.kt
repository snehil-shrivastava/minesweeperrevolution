package team.seven.minesweeper.microserver.obj

interface ITile {
    fun animate(delay: Int)
}