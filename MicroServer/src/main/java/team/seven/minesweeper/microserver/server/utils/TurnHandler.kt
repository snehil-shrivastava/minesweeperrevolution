package team.seven.minesweeper.microserver.server.utils

internal class TurnHandler(
    private val list: ArrayList<String>
    ) {

    var isGameGoingOn  = true
    private val plList = list.shuffled()
    private val playerList =  LinkedHashSet<String>(plList)
    private var mIterator = playerList.iterator()
    private var currentPlayer = mIterator.next()

    fun next(){
        if(mIterator.hasNext())
            currentPlayer = mIterator.next()
        else
            isGameGoingOn = false
    }

    fun notPlayingGame(id: String){
        playerList.remove(id)
        mIterator = playerList.iterator()
        next()
    }

    fun getCurrentPlayer() = currentPlayer

}