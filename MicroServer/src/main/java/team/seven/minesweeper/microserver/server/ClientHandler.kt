package team.seven.minesweeper.microserver.server

import team.seven.minesweeper.microserver.commons.*
import team.seven.minesweeper.microserver.obj.*
import team.seven.minesweeper.microserver.obj.ACK
import team.seven.minesweeper.microserver.obj.ServerCommands.*
import team.seven.minesweeper.microserver.util.Utils
import timber.log.Timber
import java.net.Socket
import java.text.ParseException

internal class ClientHandler(
    private val server: ServerImpl,
    private val client: Socket,
    schedulers: BaseSchedulerProvider
) : GamePlayListener, RxIOs(client.dataInputStream, client.dataOutputStream, schedulers) {


    private enum class ClientState {
        WAITING, MINE_MAP, PLAYING, FINISHED_PLAYING, DISCONNECTED
    }

    private lateinit var clientObject: ClientObject


    private lateinit var mClientState: ClientState


    init {
        reader?.subscribe(this)
    }

    private fun close() {
        client.close()
    }


    override fun plantMine(coordinates: Pair<Int, Int>) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.ADD_MINE),
                Pair("coordinates", coordinates)
            )
        )
        Timber.i("Mine added to client ${clientObject.uid} :: ")
    }

    override fun requestHintMap(coordinates: Pair<Int, Int>) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.HINT_MAP_REQUEST),
                Pair("coordinates", coordinates)
            )
        )
        Timber.i("Hint Map requested from client ${clientObject.uid} :: ")
    }

    override fun timeout() {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.TIMEOUT)
            )
        )
        Timber.i("Timeout for client ${clientObject.uid} :: ")
    }

    override fun sendMineMap(mmap: Array<Array<Int>>) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.SEND_MINE_MAP),
                Pair("mineMap", mmap)
            )
        )
        Timber.i("Forwarded mine map for client ${clientObject.uid} :: ")
    }

    override fun sendHintMap(coordinates: Pair<Int, Int>, hmap: Array<Array<Int>>) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.SEND_HINT_MAP),
                Pair("hintMap", hmap),
                Pair("coordinates", coordinates)
            )
        )
        Timber.i("Forwarded hint map for client ${clientObject.uid} :: ")
    }

    override fun notifyPlayerHasQuitted(uid: String) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.PLAYER_QUITTED),
                Pair("uid", uid)
            )
        )
        Timber.i(" Notifying $uid has quitted the game to ${clientObject.uid}:: ")
    }

    override fun sendRankList(winners: HashSet<String>, losers: HashSet<String>) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.RANK_LIST),
                Pair("winners", winners),
                Pair("losers", losers)
            )
        )
        Timber.i("Forwarded mine map for client ${clientObject.uid} :: ")
    }

    override fun successfullyJoinedGame(gameConfig: GameConfig, playerList: List<String>) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.JOIN_SUCCESS),
                Pair("gameConfig", gameConfig),
                Pair("playerList", playerList)
            )
        )
        Timber.i("Forwarded mine map for client ${clientObject.uid} :: ")
    }

    override fun sendPlantMineRequest(opponent: String) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.PLANT_MINE),
                Pair("opponent", opponent)
            )
        )
    }

    override fun waitForMove(waitCode: Wait, forPlayer: String) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.WAIT_FOR_MOVE),
                Pair("waitCode", waitCode),
                Pair("forPlayer", forPlayer)
            )
        )
    }

    override fun sendRevealTileRequest() {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.REVEAL)
            )
        )
    }

    override fun acceptPlayCommands() {
        mClientState = ClientState.PLAYING
    }

    override fun notifyPlayerHasJoined(uid: String) {
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.PLAYER_JOINED),
                Pair("id", uid)
            )
        )
    }

    override fun onStart(opponent: String) {
        mClientState = ClientState.MINE_MAP
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.PLANT_MINE_MAP),
                Pair("opponent", opponent)
            )
        )
    }

    override fun onEnd() {
        mClientState = ClientState.DISCONNECTED
        write(
            generateGsonFromVararg(
                Pair("code", ClientCode.SHOW_RESULTS)
            )
        )
        close()
    }

    override fun throwError(code: ClientErrorCodes, s: String) {
        write(
            generateGsonFromVararg(
                Pair("errorCode", code),
                Pair("message", s)
            )
        )
        Timber.e(
            Exception(s),
            "The following error has occurred with client ${clientObject.uid} :: "
        )
    }

    override fun onNext(t: String) {
        val map = Utils.gson.fromJson(t, Map::class.java) ?: return
        val code = (map["code"] as? String)?.let { ServerCommands.valueOf(it) }
        if (code == null) {
            Timber.e(ParseException("Command not found", 0))
            return
        }
        code.let {
            when {
                it == ServerCommands.ACK -> {
                    handleAcknowledgement(map)
                }
                this::clientObject.isInitialized.not() && it == JOIN -> {
                    val id = map["id"] as? String
                        ?: throw ParseException("Failed to parse client object", 10)
                    clientObject = ClientObject(id, true)
                    server.join(clientObject, this)
                }
                this::clientObject.isInitialized && it == LEAVE -> {
                    server.quit(clientObject.uid)
                    Timber.e("Client has quit :: ${clientObject.uid}")
                }
                mClientState == ClientState.MINE_MAP && it == MINE_MAP -> {
                    handleMineMapReceived(map, server)
                }
                mClientState == ClientState.PLAYING -> {
                    when (it) {
                        RANK_LIST -> {
                            server.sendRankList(clientObject.uid)
                        }
                        WON -> {
                            mClientState = ClientState.FINISHED_PLAYING
                            server.wonGame(clientObject.uid)
                        }
                        LOST -> {
                            mClientState = ClientState.FINISHED_PLAYING
                            server.lostGame(clientObject.uid)
                        }
                        PLANT -> {
                            handlePlantMine(map)
                        }
                        HINT -> {
                            val requestMaker = map["requestMaker"] as? String
                                ?: throw Exception("Cannot find requestMaker")
                            val hintMap =
                                getMap(map, "hintMap", (server).config.hintMapSize)
                            val coordinates = getCoordinates(map)
                            server.sendHintMap(requestMaker, coordinates, hintMap)
                        }
                        REQUEST_HINT -> {
                            val opponent = map["opponent"] as? String
                                ?: throw Exception("Cannot parse opponent")
                            val coordinates = getCoordinates(map)
                            server.requestHintMap(opponent, coordinates)
                        }
                        else ->
                            throwError(
                                ClientErrorCodes.INVALID_COMMAND,
                                "Not a valid command, for ${mClientState.name} state "
                            )
                    }
                }
                mClientState == ClientState.FINISHED_PLAYING -> {
                    when (it) {
                        RANK_LIST -> {
                            server.sendRankList(clientObject.uid)
                        }
                        else ->
                            throwError(
                                ClientErrorCodes.INVALID_COMMAND,
                                "Not a valid command, for ${mClientState.name} state "
                            )
                    }
                }
                else -> {
                    // Go and fuck yourself
                    Timber.e("Got in a Fucked up state")
                    throwError(ClientErrorCodes.ILLEGAL_STATE_ERROR,"Go fuck yourself, Got in a Fucked up state")
                }
            }
        }

    }

    private fun handlePlantMine(map: Map<*, *>) {
        val opponent = map["opponent"] as? String
            ?: throw Exception("Cannot parse opponent")
        val coordinates = getCoordinates(map)
        if(checkCoordinates(coordinates, server.config).not()) {
            throwError(ClientErrorCodes.INVALID_COORDINATES, ERROR_INVALID_COORDINATES)
            return
        }
        if(server.turnHandler.getCurrentPlayer() != opponent){
            throwError(ClientErrorCodes.INVALID_OPPONENT, "Opponent is not the current player")
            return
        }
        if(server.ackHandler.hasPlantedMineForCurrentOpponent(clientObject.uid)){
            throwError(ClientErrorCodes.ILLEGAL_STATE_ERROR, "Already planted Mine")
            return
        }
        server.plantMine(opponent, coordinates)
    }

    private fun handleAcknowledgement(map: Map<*, *>) {
        val ackCode = (map["ackCode"] as? String)?.let { ACK.valueOf(it) } ?: throw Exception("Cannot parse acknowledgement")
        if (server.ackHandler.handle(clientObject.uid, ackCode).not())
            throwError(ClientErrorCodes.INVALID_ACK, ERROR_INVALID_ACK)
        else
            Timber.i("acknowledged $ackCode code by ${clientObject.uid}")
    }

    private fun handleMineMapReceived(
        map: Map<*, *>,
        server: ServerImpl
    ): Boolean {
        val opponent = map["opponent"] as? String
        if (opponent == null) {
            throwError(ClientErrorCodes.INVALID_OPPONENT, "No opponent was provided")
            return false
        }
        if (opponent == clientObject.uid) {
            throwError(ClientErrorCodes.INVALID_OPPONENT, "Invalid, opponent's have same ids")
            return false
        }

        val mineMap = getMap(map, "mineMap", server.config.boardSize)
        return if (checkMineMap(mineMap, server.config)) {
            return if(server.opponentVerification.check(clientObject.uid, opponent))
            {
                server.sendMineMap(clientObject.uid, opponent, mineMap)
                true
            }
            else{
                throwError(ClientErrorCodes.INVALID_MINE_MAP, ERROR_MINE_MAP_ALREADY_RECEIVED)
                false
            }
        } else {
            throwError(ClientErrorCodes.INVALID_MINE_MAP, ERROR_INVALID_MINE_MAP)
            false
        }
    }


    override fun onError(e: Throwable) {
        Timber.e(e)
    }

    override fun onComplete() {
        TODO("Not yet implemented")
    }

    override fun onDisconnect() {
        this.onEnd()
        println("Disconnected")
    }
}