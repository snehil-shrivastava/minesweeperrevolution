package team.seven.minesweeper.microserver.commons

import com.google.gson.internal.LinkedTreeMap
import io.reactivex.rxjava3.core.ObservableEmitter
import team.seven.minesweeper.microserver.obj.GameConfig
import team.seven.minesweeper.microserver.util.Utils
import team.seven.minesweeper.microserver.util.Utils.gson
import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.Socket

val Socket.dataOutputStream: DataOutputStream
    get() {
        return DataOutputStream(getOutputStream())
    }


val Socket.dataInputStream: DataInputStream
    get() {
        return DataInputStream(getInputStream())
    }

fun getGameConfig(map: Any?): GameConfig? {
    val obj = gson.toJsonTree(map).asJsonObject;
    return gson.fromJson(obj, GameConfig::class.java)
}

fun getMap(map: Map<*, *>, key: String, size: Int): Array<Array<Int>> {
    val message = "Failed to parse MineMap"
    val test = map[key] as? ArrayList<*> ?: throw Exception(message)
    if (test.isEmpty()) throw Exception("Map is empty")
    if (test.size != size) throw Exception("Map is not according to configuration size")
    val ret = Array(test.size) { _ ->
        Array(
            (test[0] as? ArrayList<*> ?: throw Exception(message)).size
        ) { _ -> 0 }
    }
    ret.mapIndexed { i, ints ->
        ints.mapIndexed { j, _ ->
            val x = (
                    test[i] as? ArrayList<*>
                        ?: throw Exception(message))[j] as? Double
                ?: throw Exception(message)
            ret[i][j] = x.toInt()
        }
    }
    return ret
}

fun getCoordinates(map: Map<*, *>): Pair<Int, Int> {
    val e = Exception("Cannot parse coordinates")
    val coordinates = map["coordinates"] as? Map<*, *> ?: throw e
    if (coordinates["first"] is Double && coordinates["second"] is Double) {
        return Pair((coordinates["first"] as Double).toInt(), (coordinates["second"] as Double).toInt())
    } else {
        throw e
    }
}

fun getRankList(map: Map<*, *>): ArrayList<String> {
    val e = Exception("Cannot parse rank list")
    val rankList = map["rankList"] as? ArrayList<*> ?: throw e
    val toReturn = ArrayList<String>()
    rankList.forEach {
        if (it is String) {
            toReturn.add(it)
        } else {
            throw e
        }
    }
    return toReturn
}

fun <T> generateDerangementMap(list: List<T>): Map<T, T> {
    val size = list.size
    if (size < 2) throw Exception("Size is less than one cannot make a derangement")
    val map = HashMap<T, T>()
    var rand = 0
    while (rand == 0) rand = (Math.random() * size).toInt()
    list.mapIndexed { index, t ->
        map[t] = list[(index + rand) % size]
    }
    return map
}


fun <T> ObservableEmitter<T>.onSafeError(e: Exception) {
    if (this.isDisposed.not())
        this.onError(e)
}

fun checkCoordinates(coordinate: Pair<Int, Int>, gameConfig: GameConfig) =
    coordinate.first.coerceIn(0 until gameConfig.boardSize) == coordinate.first && coordinate.second.coerceIn(
        0 until gameConfig.boardSize
    ) == coordinate.second

fun checkMineMap(map: Array<Array<Int>>, config: GameConfig): Boolean =
    (map.size == config.boardSize && map.isNotEmpty() && map[0].size == config.boardSize && config.noOfMinesToBePlanted == map.sumBy { it.sum() })

inline fun <reified INNER> array2d(
    sizeOuter: Int,
    sizeInner: Int,
    noinline innerInit: (Int) -> INNER
): Array<Array<INNER>> = Array(sizeOuter) { Array<INNER>(sizeInner, innerInit) }

fun array2dOfInt(sizeOuter: Int, sizeInner: Int): Array<IntArray> =
    Array(sizeOuter) { IntArray(sizeInner) }

fun array2dOfLong(sizeOuter: Int, sizeInner: Int): Array<LongArray> =
    Array(sizeOuter) { LongArray(sizeInner) }

fun array2dOfByte(sizeOuter: Int, sizeInner: Int): Array<ByteArray> =
    Array(sizeOuter) { ByteArray(sizeInner) }

fun array2dOfChar(sizeOuter: Int, sizeInner: Int): Array<CharArray> =
    Array(sizeOuter) { CharArray(sizeInner) }

fun array2dOfBoolean(sizeOuter: Int, sizeInner: Int): Array<BooleanArray> =
    Array(sizeOuter) { BooleanArray(sizeInner) }

fun generateGsonFromVararg(vararg objects: Pair<String, Any>): String {
    val map = HashMap<String, Any>()
    objects.forEach {
        map[it.first] = it.second
    }
    return Utils.gson.toJson(map)
}


const val ERROR_JOIN_GAME_ON = "Cannot join the server when game is already going on"
const val ERROR_JOIN_GAME_FINISHED = "Cannot join the server when game is already finished"
const val ERROR_JOIN_ALREADY_JOINED = "You have all ready joined"
const val ERROR_JOIN_FULL = "Server is all ready full"
const val ERROR_CLOSE_FORCED = "Host player has probably balled on you"
const val ERROR_INVALID_COORDINATES = "Coordinates are not within bounds"
const val ERROR_INVALID_MINE_MAP = "Mine map is not valid"
const val ERROR_MINE_MAP_ALREADY_RECEIVED = "Mine map is already recived"
const val ERROR_INVALID_ACK = "Invalid acknowledgement"