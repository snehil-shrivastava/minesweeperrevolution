package team.seven.minesweeper.microserver.server

import com.nhaarman.mockitokotlin2.KArgumentCaptor
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.mockito.Mockito.*
import team.seven.minesweeper.microserver.commons.*
import team.seven.minesweeper.microserver.obj.*
import team.seven.minesweeper.microserver.server.utils.TurnHandler


class ServerImplTest {


    @Test
    fun testJoin() {
        // Arrange
        val gameConfig = GameConfig(2, 8, 3, 0, 1, 4)
        server.initialize(gameConfig)
        val a = mock(GamePlayListener::class.java)
        val b = mock(GamePlayListener::class.java)
        val c = mock(GamePlayListener::class.java)

        // Act
        server.join(ClientObject("a", false), a)
        server.join(ClientObject("a", false), a)
        server.join(ClientObject("b", false), b)
        server.join(ClientObject("c", false), c)


        // Assert
        val gameConfigReceived = argumentCaptor<GameConfig>()
        val plList = argumentCaptor<List<String>>()
        verify(a, times(1)).successfullyJoinedGame(capture(gameConfigReceived), capture(plList))
        verify(b, times(1)).successfullyJoinedGame(capture(gameConfigReceived), capture(plList))

        val errorCode = argumentCaptor<ClientErrorCodes>()
        val message = argumentCaptor<String>()
        val opponents = argumentCaptor<String>()


        verify(a, times(1)).throwError(capture(errorCode), capture(message))
        verify(c, times(1)).throwError(capture(errorCode), capture(message))

        verify(a, times(1)).onStart(capture(opponents))
        verify(b, times(1)).onStart(capture(opponents))

        val capturedErrors = errorCode.allValues
        val capturedMessage = message.allValues
        val capturedOpponents = opponents.allValues

        assertEquals(gameConfig, gameConfigReceived.allValues[0])
        assertEquals(gameConfig, gameConfigReceived.allValues[1])
        assertEquals(ClientErrorCodes.CANNOT_JOIN, capturedErrors[0])
        assertEquals(ClientErrorCodes.SERVER_FULL, capturedErrors[1])

        assertEquals(ERROR_JOIN_ALREADY_JOINED, capturedMessage[0])
        assertEquals(ERROR_JOIN_FULL, capturedMessage[1])

        assertEquals("a", capturedOpponents[1])
        assertEquals("b", capturedOpponents[0])
    }

    @Test
    fun testQuit() {
        // Arrange
        val (a, b) = arrange()

        // Act
        server.quit("a")
        server.quit("a")
        server.quit("b")

        // Assert
        val player = argumentCaptor<String>()
        verify(b, times(1)).notifyPlayerHasQuitted(capture(player))
        verify(a, times(0)).notifyPlayerHasQuitted(capture(player))

        assertEquals("a", player.firstValue)
    }

    @Test
    fun testClose() {
        // Arrange
        val (a, b) = arrange()

        // Act
        server.gameState = ServerImpl.GameState.FINISHED
        server.close()
        server.gameState = ServerImpl.GameState.PLAYING
        server.close()

        // Assert
        val code = argumentCaptor<ClientErrorCodes>()
        val error = argumentCaptor<String>()
        verify(a, times(1)).onEnd()
        verify(b, times(1)).onEnd()
        verify(b, times(1)).throwError(capture(code), capture(error))
        verify(a, times(1)).throwError(capture(code), capture(error))

        assertEquals(ClientErrorCodes.FORCE_CLOSED, code.firstValue)
        assertEquals(ERROR_CLOSE_FORCED, error.firstValue)
    }

    @Test
    fun testPlantMine() {
        // Arrange
        val (_, b) = arrange()
        val cCoordinate = Pair(7, 7)

        // Act
        server.plantMine("b", cCoordinate)

        // Assert
        val coordinates = argumentCaptor<Pair<Int, Int>>()
        verify(b, times(1)).plantMine(coordinates.capture())

        assertEquals(cCoordinate, coordinates.firstValue)
    }


    // ? for ack client receiver should receive all the mine map or plant for each received value it should send back an ack.
    // ? If received item is incorrect then it should send error to server.
    // ? The server should also check the item that being send.
    // * Here each function have only the role to pass it to client opponent without check
    // * check should exists only in onNext function of client handler
    @Test
    fun testMineMap() {
        // Arrange
        val (a, _) = arrange()
        var cnt = 0
        val correctMap = Array(8) {
            Array(8) {
                if (cnt < 20) {
                    cnt++
                    1
                } else {
                    0
                }
            }
        }
        // Act
        server.sendMineMap("a", "b", correctMap)

        // Assert
        val mMap = argumentCaptor<Array<Array<Int>>>()
        verify(a, times(0)).sendMineMap(capture(mMap))
    }

    @Test
    fun testSendHintMap(){
        val (a, _) = arrange()
        var cnt = 0
        val correctMap = Array(3) {
            Array(3) {
                if (cnt < 5) {
                    cnt++
                    1
                } else {
                    0
                }
            }
        }
        val coordinate = Pair(0, 0)
        // Act
        server.sendHintMap("a", coordinate, correctMap)
        server.sendHintMap("x", coordinate, correctMap)

        // Assert
        val mMap = argumentCaptor<Array<Array<Int>>>()
        val coordinates = argumentCaptor<Pair<Int, Int>>()
        verify(a, times(1)).sendHintMap(capture(coordinates), capture(mMap))

        assertEquals(coordinate, coordinates.firstValue)
        assertEquals(correctMap, mMap.firstValue)
    }

    @Test
    fun testLostAndWon(){
        val (a, _) = arrange()
        val winnersc = HashSet<String>()
        val loosersc = HashSet<String>()
        winnersc.add("a")
        loosersc.add("b")

        // Act

        server.gameState = ServerImpl.GameState.PLAYING
        server.wonGame("a")
        server.lostGame("a")
        server.lostGame("b")
        server.lostGame("a")
        server.wonGame("b")
        server.wonGame("a")
        server.sendRankList("a")
        // Assert
        val winners = argumentCaptor<HashSet<String>>()
        val losers = argumentCaptor<HashSet<String>>()
        verify(a, times(1)).sendRankList(capture(winners), capture(losers))

        assertEquals(winnersc, winners.firstValue)
        assertEquals(loosersc, losers.firstValue)
    }


    private fun arrange(): Pair<GamePlayListener, GamePlayListener> {
        val a = mock(GamePlayListener::class.java)
        val b = mock(GamePlayListener::class.java)

        val gameConfig = GameConfig(2, 8, 3, 20, 1, 4)
        server.initialize(gameConfig)
        server.join(ClientObject("a", false), a)
        server.join(ClientObject("b", false), b)
        return Pair(a, b)
    }


    private lateinit var server: ServerImpl

    @BeforeEach
    fun setup() {
        server =
            ServerImpl(TestSchedulerProvider(TestSchedulerRequester()), NgrokProp("", "", "")) {

            }
        server = spy(server)
    }

    @AfterEach
    fun close() {
        server.close()
    }

}

fun <T> capture(argumentCaptor: KArgumentCaptor<T>): T = argumentCaptor.capture()

