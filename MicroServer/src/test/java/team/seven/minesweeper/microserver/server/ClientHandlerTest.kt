package team.seven.minesweeper.microserver.server

import com.nhaarman.mockitokotlin2.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.internal.verification.VerificationModeFactory.times
import team.seven.minesweeper.microserver.commons.TestSchedulerProvider
import team.seven.minesweeper.microserver.commons.TestSchedulerRequester
import team.seven.minesweeper.microserver.commons.generateGsonFromVararg
import team.seven.minesweeper.microserver.obj.*
import team.seven.minesweeper.microserver.util.Utils
import java.io.PipedOutputStream
import kotlin.random.Random

class ClientHandlerTest {

    private lateinit var clientHandler: ClientTestHelper

    private lateinit var server: ServerImpl
    private val gameConfig = GameConfig(
        3,
        8,
        2,
        20,
        4,
        4
    )

    @BeforeEach
    fun setup() {
        server = ServerImpl(TestSchedulerProvider(TestSchedulerRequester()), NgrokProp("", "", "")) {}
        server.initialize(gameConfig)
        clientHandler = ClientTestHelper(server, "A", "B", "C")
    }

    @Test
    fun testJoin() {
        clientHandler.joinAll()
        val x = generateGsonFromVararg(
            Pair("code", ClientCode.JOIN_SUCCESS),
            Pair("gameConfig", gameConfig),
            Pair("playerList", arrayOf("A"))
        )
        clientHandler.capture(0, 4)
        clientHandler.test(x, 0)
    }

    @Test
    fun `when all the players have joined the game but the third player have same id`() {
        clientHandler.joinAll()
        clientHandler.join(0)
        clientHandler.capture(0, 5)
        clientHandler.test(
            "{\"errorCode\":\"ILLEGAL_STATE_ERROR\",\"message\":\"Go fuck yourself, Got in a Fucked up state\"}",
            0
        )
    }

    @Test
    fun `when all the players have correctly joined the game`() {
        clientHandler.joinAll()
        clientHandler.capture(0, 4)
        clientHandler.capture(1, 3)
        clientHandler.capture(2, 2)

        clientHandler.test("{\"code\":\"PLAYER_JOINED\",\"id\":\"C\"}", 0)
        clientHandler.test("{\"code\":\"PLAYER_JOINED\",\"id\":\"B\"}", 0)
        clientHandler.test("{\"code\":\"PLAYER_JOINED\",\"id\":\"C\"}", 1)

        assertNotEquals("A", clientHandler.opponent(0))
        assertNotEquals("B", clientHandler.opponent(1))
        assertNotEquals("C", clientHandler.opponent(2))
    }


    @Test
    fun `After joining sending mine map twice`() {
        clientHandler.joinAll()
        clientHandler.capture(0, 4)
        clientHandler.capture(1, 3)
        clientHandler.capture(2, 2)

        val oa = clientHandler.opponent(0)
        val ob = clientHandler.opponent(1)
        val oc = clientHandler.opponent(2)

        assertNotEquals(oa, ob)
        assertNotEquals(oc, oa)
        assertNotEquals(oc, ob)
        assertNotEquals("A", oa)
        assertNotEquals("B", ob)
        assertNotEquals("C", oc)
        // Act
        var a = getMineMapString(oa, 8)
        var b = getMineMapString(ob, 4)
        var c = getMineMapString(oc, 20)

        clientHandler.next(0, a)
        clientHandler.next(1, b)
        clientHandler.next(2, c)


        a = getMineMapString(oa, 20)
        b = getMineMapString(ob, 20)
        c = getMineMapString(oc, 20)

        clientHandler.next(0, a)
        clientHandler.next(1, b)
        clientHandler.next(2, c)
        a = generateGsonFromVararg(
            Pair("code", ServerCommands.ACK),
            Pair("ackCode", ACK.RECEIVED_MINE_MAP),
        )


        clientHandler.next(0, a)
        clientHandler.next(1, a)
        clientHandler.next(2, a)
    }

    @Test
    fun `player lost but checking rank list`() {
        val rand = Random((Math.random() * 1e9).toInt())
        val mockedTrigger : (String) -> String? =  mockedTrigger@ { t ->
            val map = Utils.gson.fromJson(t, Map::class.java) ?: return@mockedTrigger null
            val code = (map["code"] as? String)?.let { ClientCode.valueOf(it) } ?: return@mockedTrigger null
            val opponent = (map["opponent"] as? String) ?: return@mockedTrigger null
            when(code) {
                ClientCode.PLANT_MINE -> {
                    return@mockedTrigger generateGsonFromVararg(
                        Pair("code", ServerCommands.PLANT),
                        Pair("opponent", opponent),
                        Pair("coordinates", Pair(rand.nextInt(8), rand.nextInt(8)))
                    )
                }
                else -> return@mockedTrigger  null
            }
        }
        clientHandler.triggerOnWrite(0, mockedTrigger)
        clientHandler.triggerOnWrite(1, mockedTrigger)
        clientHandler.triggerOnWrite(2, mockedTrigger)
        `After joining sending mine map twice`()
        clientHandler.clearCaptures()

        clientHandler.next(0, generateGsonFromVararg(
            Pair("code", ServerCommands.LOST)
        ))
        clientHandler.next(0, generateGsonFromVararg(
            Pair("code", ServerCommands.RANK_LIST)
        ))
        val penalty = HashMap<String, Int>()
//        server.startPlaying()
        server.executeGameLoop(penalty)
        clientHandler.capture(0)
        clientHandler.test("{\"winners\":[],\"code\":\"RANK_LIST\",\"losers\":[\"A\"]}", 0)
    }

    private fun getMineMapString(opponent: String, bombs: Int): String {
        val map = HashMap<String, Any>()
        map["code"] = ServerCommands.MINE_MAP
        map["mineMap"] = arrayOfArrays(bombs)
        map["opponent"] = opponent
        return Utils.gson.toJson(map)
    }

    private fun arrayOfArrays(cnti: Int): Array<Array<Int>> {
        var cnt = 0
        return Array(8) {
            Array(8) {
                if (cnt < cnti) {
                    cnt = cnt.inc()
                    1
                } else 0
            }
        }
    }

    @AfterEach
    fun close() {
        server.close()
    }

    internal class ClientTestHelper(private val server: ServerImpl, vararg ids: String) {
        private val clientHandler = ArrayList<ClientHandler>()
        private val argsCaps = ArrayList<KArgumentCaptor<String>>()

        val id: List<String>

        init {
            ids.forEachIndexed { _, _ ->
                val x = ClientHandler(server, mock() {
                    on { getOutputStream() } doReturn PipedOutputStream()
                    on { getInputStream() } doReturn "".byteInputStream()
                }, TestSchedulerProvider(TestSchedulerRequester()))
                clientHandler.add(spy(x))
                argsCaps.add(argumentCaptor())
            }
            id = ids.toList()
        }

        fun joinAll() {
            clientHandler.forEachIndexed { i, x ->
                val json = generateGsonFromVararg(
                    Pair("code", ServerCommands.JOIN),
                    Pair("id", id[i])
                )
                x.onNext(json)
            }
        }

        fun join(i: Int) {
            val json = generateGsonFromVararg(
                Pair("code", ServerCommands.JOIN),
                Pair("id", id[i])
            )
            clientHandler[i].onNext(json)
        }

        fun test(string: String, i: Int) {
            assertThat(
                argsCaps[i].allValues, hasItem(
                    string
                )
            )
        }

        fun opponent(i: Int): String =
            Utils.gson.fromJson(argsCaps[i].lastValue, Map::class.java)["opponent"] as String

        fun capture(i: Int, times: Int) {
            verify(clientHandler[i], times(times)).write(argsCaps[i].capture())
        }

        fun capture(i: Int) {
            verify(clientHandler[i], atLeast(1)).write(argsCaps[i].capture())
        }

        fun triggerOnWrite(id: Int, func : (String) -> String?){
            whenever(clientHandler[id].write(any())).thenAnswer {
                val log = it.getArgument<String>(0)
                func(log)?.let { it1 -> clientHandler[id].onNext(it1) }
            }
        }

        fun next(i: Int, c: String) {
            clientHandler[i].onNext(c)
        }

        fun clearCaptures() {
            val size = argsCaps.size
            argsCaps.clear()
            for (i in 0 until size)
                argsCaps.add(argumentCaptor())
        }

        fun printAll(i: Int) {
            argsCaps[i].allValues.forEach { println(it) }
        }
    }


}

