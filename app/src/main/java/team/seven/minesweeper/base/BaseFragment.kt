package team.seven.minesweeper.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.ProgressBar
import androidx.annotation.CallSuper
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import team.seven.minesweeper.R
import team.seven.minesweeper.ui.MainActivity

/**
 * This fragment uses a view stub before displaying contents
 * So a progress bar is shown before inflating the layout
 *
 *  @param T is the data binding class
 */

abstract class BaseFragment<T: ViewDataBinding> : Fragment() {

    val _activity by lazy { activity as MainActivity }

    lateinit var binding: T

    private var mSavedInstanceState: Bundle? = null
    private var hasInflated = false
    private lateinit var mViewStub: ViewStub
    private var visible = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_viewstub, container, false)
        mViewStub = view.findViewById(R.id.fragmentViewStub) as ViewStub
        mViewStub.layoutResource = layout
        mViewStub.setOnInflateListener(this::setupBinding)
        mSavedInstanceState = savedInstanceState

        if (visible && !hasInflated) {
            val inflatedView = mViewStub.inflate()
            onCreateViewAfterViewStubInflated(inflatedView, mSavedInstanceState)
            afterViewStubInflated(view)
        }

        return view
    }

    private fun setupBinding(stub: ViewStub, view: View) {
        DataBindingUtil.bind<T>(view)?.let{
            binding = it
        }
    }

    protected abstract fun onCreateViewAfterViewStubInflated(
        inflatedView: View,
        savedInstanceState: Bundle?
    )

    /**
     * The layout ID associated with this ViewStub
     * @see ViewStub.setLayoutResource
     * @return
     */
    protected abstract val layout: Int

    /**
     *
     * @param originalViewContainerWithViewStub
     */
    @CallSuper
    protected fun afterViewStubInflated(originalViewContainerWithViewStub: View?) {
        hasInflated = true
        if (originalViewContainerWithViewStub != null) {
            val pb =
                originalViewContainerWithViewStub.findViewById<ProgressBar>(R.id.inflateProgressbar)
            pb.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()

        visible = true
        if (!hasInflated && this::mViewStub.isInitialized) {
            val inflatedView = mViewStub.inflate()
            onCreateViewAfterViewStubInflated(inflatedView, mSavedInstanceState)
            afterViewStubInflated(view)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hasInflated = false
    }

    override fun onPause() {
        super.onPause()
        visible = false
    }

    // Thanks to Noa Drach, this will fix the orientation change problem
    override fun onDetach() {
        super.onDetach()
        hasInflated = false
    }
}