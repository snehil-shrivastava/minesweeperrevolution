package team.seven.minesweeper

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MineApp : Application(){
}