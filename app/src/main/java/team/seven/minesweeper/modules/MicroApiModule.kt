package team.seven.minesweeper.modules

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import team.seven.minesweeper.microserver.commons.BaseSchedulerProvider
import team.seven.minesweeper.microserver.commons.SchedulerProvider

@Module
@InstallIn(SingletonComponent::class)
class MicroApiModule {


//    @Singleton
//    @Provides
//    fun server(
//        database: FirebaseDatabase,
//        baseSchedulerProvider: BaseSchedulerProvider,
//        app: Application
//    ): Server {
//        return MicroApi.getServer(
//            baseSchedulerProvider,
//            NgrokProp(
//                "${app.applicationInfo.nativeLibraryDir}/ngrok.sh",
//                app.externalCacheDir?.path ?: app.filesDir.path,
//                "" // TODO auth token
//            )
//        ){
//            val xpath = database.getReference("servers").child("")
//            xpath.child("url").setValue(it.first)
//            xpath.child("port").setValue(it.second)
//        }
//    }
//
//    @Singleton
//    @Provides
//    fun client(
//        baseSchedulerProvider: BaseSchedulerProvider
//    ): Client {
//            return MicroApi.getClient(baseSchedulerProvider)
//    }

    @Provides
    fun getSP() = SchedulerProvider() as BaseSchedulerProvider
}