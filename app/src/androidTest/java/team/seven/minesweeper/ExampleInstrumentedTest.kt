package team.seven.minesweeper

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import team.seven.minesweeper.microserver.MicroApi
import team.seven.minesweeper.microserver.commons.SchedulerProvider
import team.seven.minesweeper.microserver.obj.ClientObject
import team.seven.minesweeper.microserver.obj.GameConfig
import team.seven.minesweeper.microserver.obj.NgrokProp

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {


    @Test
    fun useAppContext() {
        // Context of the app under test.
        val app = InstrumentationRegistry.getInstrumentation().targetContext
        val gameConfig = GameConfig(
            2,
            8,
            4,
            20
        )
        val ngrokProp = NgrokProp(
            "${app.applicationInfo.nativeLibraryDir}/ngrok.sh",
            app.externalCacheDir?.path ?: app.dataDir.path,
            "1mNTeUMXQ3s0c1sARFaUCbEMHOR_4NmxEDvxWVjwb8e76rGDH"
        )
        val clientObj = ClientObject(
            "A",
            true
        )
        var i: Int = 0
        val client = MicroApi.getClient(SchedulerProvider())
        val gameController = MicroApi.getGameController(client, clientObj)
        val server = MicroApi.getServer(gameConfig, SchedulerProvider(), ngrokProp){
            client.initialize(it, gameController){
                client.join(clientObj)
                i = 1
            }
        }
        server.initialize()
        while(i==0) Thread.sleep(100)
    }
}