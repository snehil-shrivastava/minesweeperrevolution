# Contributing

1. Create an issue or use an existing open issue.
2. Assign yourself to the issue
3. Create a new branch with issue no eg - I123
4. Create a new MR after making the required changes to resolve the issue.
